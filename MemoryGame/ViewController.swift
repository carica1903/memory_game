//
//  ViewController.swift
//  MemoryGame
//
//  Created by Pascal Adam on 13.11.18.
//  Copyright © 2018 Pascal Adam. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    //lazy wird erst initialisiert wenn sie wirklich einer benutzt
    
//    var flipCount = 0 {
//        //Property observer called everytime the variable≠ is set
//        didSet {
//            flipCountLabel.text = "Flips: \(flipCount)"
//        }
//    }
    
    var cardThemes = [["🐶","🐱","🐭","🦊","🦄","🐷","🐳","🐵"],
                      ["🍏","🥦","🥨","🥐","🍔","🍕","🍣","🥙"],
                      ["⚽️","🛹","🏓","🏒","🎱","🏄‍♂️","🚴‍♂️","🏋️‍♀️"],
                      ["🚗","🚌","🚝","🛩","🛳","🚦","🛴","🚂"],
                      ["🏴‍☠️","🏳️‍🌈","🇩🇪","🇫🇷","🇪🇺","🇨🇦","🇨🇺","🏁"],
                      ["👻","🎃","🦇","☠️","👹","👽","🧟‍♀️","🧙‍♀️"]]
    
    //    var emojiChoises = ["👻","🎃","🦇","☠️","👹","👽","🧟‍♀️","🧙‍♀️"]
    //    lazy var emojiChoises = [String]()
    lazy var emojiChoises = cardThemes.randomElement()!
    
    var emoji = Dictionary<Int, String>()
    //geht auch so --> var emoji = [Int:String]()
    
    // MARK: bla bla
    // TODO: something
    
    // [ = option+5
    // ] = option+6
    @IBOutlet var cardButtons: [UIButton]!
    
    @IBOutlet weak var scoreCountLabel: UILabel!
    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBAction func touchReset(_ sender: UIButton) {
        //        print("Reset button touched")
        game.reset()
        emojiChoises = cardThemes.randomElement()!
        updateViewFromModel()
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        //flipCount += 1
        
        //With Swift 4.2, index is no longer used but is separated into firstIndex and lastIndex for better clarification. So depending on whether you are looking for the first or last index of the item:
        // with the "if" it is checket if the "optional" (Int?) return value is there (the index of the button) or not available (nil)
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            //flipCard(withEmoji: emojiChoises[cardNumber], on: sender)
            //            print("cardNumber = \(cardNumber)")
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("Card is not in cardButtons")
        }
    }
    
    func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
        flipCountLabel.text = "Flips: \(game.flipCount)"
        scoreCountLabel.text = "Score: \(game.score)"
    }
    
    func emoji(for card: Card) -> String
    {
        if emoji[card.identifier] == nil
        {
            if emojiChoises.count > 0
            {
                let randomIndex = Int(arc4random_uniform(UInt32(emojiChoises.count)))
                emoji[card.identifier] = emojiChoises.remove(at: randomIndex)
            }
        }
        if emoji[card.identifier] != nil
        {
            return emoji[card.identifier]!
        }
        else
        {
            return "?"
        }
        // return emoji[card.identifier] ?? "?" // macht das gleiche wie das if oben
    }
}

