//
//  MemoryModel.swift
//  MemoryGame
//
//  Created by Pascal Adam on 21.11.18.
//  Copyright © 2018 Pascal Adam. All rights reserved.
//

import Foundation

class Concentration
{
    var cards = [Card]() //Array of Card
    var indexOfOneAndOnlyFaceUpCard: Int?
    let positiveScore = 2
    let penaltyScore = 1
    
    var score = 0 {
        didSet {
            print("scrore = \(score)")
        }
    }
    
    var flipCount = 0 {
        //Property observer called everytime the variable is set
        didSet {
            print("scrore = \(score)")
        }
    }
    
    func chooseCard(at index: Int)
    {
        flipCount += 1
        
        if !cards[index].isMatched
        {
            // 2nd card touched
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index
            {
                // flip 2nd card
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = nil
                
                // check if cards match
                if cards[matchIndex].identifier == cards[index].identifier
                {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    score += positiveScore
                }else
                {
                    // check if cards has been turned around before and if not yet done                    
                    if cards[index].hasBeenTurnedAroundBefore{
                        score -= penaltyScore
                    }
                    else{
                        cards[index].hasBeenTurnedAroundBefore = true
                    }
                    
                    if cards[matchIndex].hasBeenTurnedAroundBefore{
                        score -= penaltyScore
                    }
                    else{
                        cards[matchIndex].hasBeenTurnedAroundBefore = true
                    }
                }
            }
                
            else // no cards or 1 card face up
            {
                // make sure that all cards are face down
                for flipDownIndex in cards.indices
                {
                    cards[flipDownIndex].isFaceUp = false
                }
                // just flip the first card
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    func reset()
    {
        for resetIndex in cards.indices
        {
            cards[resetIndex].isFaceUp = false
            cards[resetIndex].isMatched = false
            cards[resetIndex].hasBeenTurnedAroundBefore = false
        }
        indexOfOneAndOnlyFaceUpCard = nil
        score = 0
        flipCount = 0
        cards.shuffle()
    }
    
    init(numberOfPairsOfCards: Int)
    {
        for _ in 1...numberOfPairsOfCards {
            
            let card = Card()
            //each card is needed twice ;)
            cards += [card, card]
            
            // Shuffel the cards
            cards.shuffle()
        }
    }
}
