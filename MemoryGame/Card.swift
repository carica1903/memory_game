//
//  Card.swift
//  MemoryGame
//
//  Created by Pascal Adam on 21.11.18.
//  Copyright © 2018 Pascal Adam. All rights reserved.
//

import Foundation

struct Card
{
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    var hasBeenTurnedAroundBefore = false
    
    static var identifierFactory = 0
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
}
